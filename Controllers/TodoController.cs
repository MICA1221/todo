﻿using Microsoft.AspNetCore.Mvc;
using ToDO.Models;
using ToDO.Services;

namespace ToDO.Controllers
{

    public class TodoController : Controller
    {
        private TodoListService _todoListService;

        public TodoController()
        {
            _todoListService = TodoListService.Instance;
        }

        public IActionResult Index()
        {
            var todoItems = _todoListService.GetTodoItems();
            return View(todoItems);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(TodoItem newItem)
        {
            if (ModelState.IsValid)
            {
                newItem.Id = _todoListService.GetTodoItems().Count + 1;
                _todoListService.AddTodoItem(newItem);
                return RedirectToAction(nameof(Index));
            }

            return View(newItem);
        }
        [HttpPost]
        public IActionResult Delete(int id)
        {
            _todoListService.RemoveTodoItem(id);
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public IActionResult Toggle(int id)
        {
            _todoListService.ToggleTodoItem(id);
            return RedirectToAction(nameof(Index));
        }

    }

}
