﻿using ToDO.Models;

namespace ToDO.Services
{
    public class TodoListService
    {
        private static TodoListService _instance;
        private List<TodoItem> _todoItems;

        private TodoListService()
        {
            _todoItems = new List<TodoItem>();
        }

        public static TodoListService Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new TodoListService();
                }
                return _instance;
            }
        }

        public List<TodoItem> GetTodoItems()
        {
            return _todoItems;
        }

        public void AddTodoItem(TodoItem item)
        {
            _todoItems.Add(item);
        }

        public void RemoveTodoItem(int id)
        {
            _todoItems.RemoveAll(item => item.Id == id);
        }

        public void ToggleTodoItem(int id)
        {
            var item = _todoItems.FirstOrDefault(i => i.Id == id);
            if (item != null)
            {
                item.Completed = !item.Completed;
            }
        }
    }


}
