﻿namespace ToDO.Models
{
    public class TodoItem
    {
        public int Id { get; set; }
        public string Task { get; set; }
        public bool Completed { get; set; }
    }
}
